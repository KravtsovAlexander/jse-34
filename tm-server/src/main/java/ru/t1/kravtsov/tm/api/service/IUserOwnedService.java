package ru.t1.kravtsov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IUserOwnedRepository;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M>, IUserOwnedRepository<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
