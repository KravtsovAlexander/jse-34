package ru.t1.kravtsov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IUserOwnedRepository;
import ru.t1.kravtsov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    protected Predicate<M> filterByUserId(@NotNull final String userId) {
        return m -> userId.equals(m.getUserId());
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        deleteAll(models);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .filter(filterById(id))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null) return 0;
        return (int) findAll()
                .stream()
                .filter(filterByUserId(userId))
                .count();
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public void deleteAll(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final List<M> models = findAll(userId);
        for (@NotNull M model : models) {
            remove(userId, model);
        }
    }

}
