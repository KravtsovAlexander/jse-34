package ru.t1.kravtsov.tm.endpoint;

import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kravtsov.tm.api.service.IServiceLocator;
import ru.t1.kravtsov.tm.dto.request.*;
import ru.t1.kravtsov.tm.dto.response.*;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Session;
import ru.t1.kravtsov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@WebService(endpointInterface = "ru.t1.kravtsov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/bindTaskToProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskBindToProjectResponse bindTaskToProject(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/changeTaskStatusById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/changeTaskStatusByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getServiceLocator().getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/clearTask")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskClearResponse clearTask(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getServiceLocator().getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/completeTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskCompleteByIdResponse completeTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/completeTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/createTask")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskCreateResponse createTask(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getServiceLocator().getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/displayTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskDisplayByIdResponse displayTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskDisplayByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getServiceLocator().getTaskService().findOneById(userId, id);
        return new TaskDisplayByIdResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/displayTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskDisplayByIndexResponse displayTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskDisplayByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getServiceLocator().getTaskService().findOneByIndex(userId, index);
        return new TaskDisplayByIndexResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/listTaskByProjectId")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskListByProjectIdResponse listTaskByProjectId(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListByProjectIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        if (userId == null || projectId == null) {
            return new TaskListByProjectIdResponse(null);
        }
        @Nullable final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/listTask")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskListResponse listTask(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Task> tasks = getServiceLocator().getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/removeTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskRemoveByIdResponse removeTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getServiceLocator().getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/removeTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getServiceLocator().getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/removeTaskByName")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskRemoveByNameResponse removeTaskByName(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByNameRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        if (userId == null || name == null) {
            return new TaskRemoveByNameResponse(null);
        }
        @Nullable final List<Task> tasks = getServiceLocator().getTaskService().removeByName(userId, name);
        return new TaskRemoveByNameResponse(tasks);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/startTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskStartByIdResponse startTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/startTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskStartByIndexResponse startTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getServiceLocator().getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/unbindTaskFromProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @NotNull final Task task = getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/updateTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskUpdateByIdResponse updateTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/updateTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
