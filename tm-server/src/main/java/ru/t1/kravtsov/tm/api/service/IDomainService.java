package ru.t1.kravtsov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    Domain getDomain();

    void setDomain(@Nullable final Domain domain);

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void saveDataJsonFasterXml();

    void loadDataJsonJaxB();

    void saveDataJsonJaxB();

    void loadDataXmlFasterXml();

    void saveDataXmlFasterXml();

    void loadDataXmlJaxB();

    void saveDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

}
