package ru.t1.kravtsov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.comparator.NameComparator;
import ru.t1.kravtsov.tm.comparator.StatusComparator;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.marker.UnitCategory;
import ru.t1.kravtsov.tm.model.Project;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final Project alphaProject = new Project("alpha", "testProject");

    @NotNull
    private final Project betaProject = new Project("beta", "testProject");

    @NotNull
    private final Project gammaProject = new Project("gamma", "testProject");

    {
        alphaProject.setStatus(Status.COMPLETED);
        alphaProject.setUserId("user1");
        alphaProject.setId("alpha-project-id");
        betaProject.setStatus(Status.IN_PROGRESS);
        betaProject.setUserId("user2");
        betaProject.setId("beta-project-id");
        gammaProject.setStatus(Status.NOT_STARTED);
        gammaProject.setUserId("user1");
        gammaProject.setId("gamma-project-id");
    }

    @Before
    public void before() {
        repository.add(alphaProject);
        repository.add(betaProject);
        repository.add(gammaProject);
    }

    @After
    public void after() {
        repository.clear();
    }

    @Test
    public void add() {
        final int initialSize = repository.getSize();
        Assert.assertNotNull(repository.add(new Project()));
        Assert.assertEquals(initialSize + 1, repository.getSize());
    }

    @Test
    public void addCollection() {
        final int initialSize = repository.getSize();
        @NotNull final List<Project> projects = Arrays.asList(new Project(), new Project());
        Assert.assertNotNull(repository.add(projects));
        Assert.assertEquals(initialSize + projects.size(), repository.getSize());
    }

    @Test
    public void addForUser() {
        final int initialSize = repository.getSize();
        @NotNull final Project project = new Project();
        Assert.assertNotNull(repository.add("user1", project));
        Assert.assertEquals(initialSize + 1, repository.getSize());
        Assert.assertEquals("user1", project.getUserId());

        Assert.assertNull(repository.add(null, new Project()));
        Assert.assertNull(repository.add("user1", null));
    }

    @Test
    public void clear() {
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void clearForUser() {
        repository.clear("user1");
        Assert.assertEquals(1, repository.getSize());
        Assert.assertSame(betaProject, repository.findAll().get(0));
    }

    @Test
    public void deleteAll() {
        repository.deleteAll();
        Assert.assertEquals(0, repository.getSize());

    }

    @Test
    public void deleteAllSpecificProjects() {
        @NotNull final List<Project> projects = Arrays.asList(alphaProject, betaProject);
        repository.deleteAll(projects);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertSame(gammaProject, repository.findAll().get(0));
    }

    @Test
    public void deleteAllUserProjects() {
        repository.deleteAll("user1");
        Assert.assertEquals(1, repository.getSize());
        Assert.assertSame(betaProject, repository.findAll().get(0));

        @Nullable final String nullId = null;
        repository.deleteAll(nullId);
        Assert.assertEquals(1, repository.getSize());

    }

    @Test
    public void findAll() {
        @NotNull List<Project> projects = repository.findAll();
        @NotNull List<Project> expected = Arrays.asList(alphaProject, betaProject, gammaProject);
        Assert.assertEquals(expected, projects);

        repository.clear();
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    public void findAllUserProjects() {
        @NotNull List<Project> projects = repository.findAll("user1");
        @NotNull List<Project> expected = Arrays.asList(alphaProject, gammaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void findAllWithComparator() {
        repository.clear();
        repository.add(betaProject);
        repository.add(gammaProject);
        repository.add(alphaProject);

        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        @NotNull List<Project> projects = repository.findAll(comparator);
        @NotNull List<Project> expected = Arrays.asList(alphaProject, betaProject, gammaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void findAllUserProjectsWithComparator() {
        @NotNull final Comparator comparator = StatusComparator.INSTANCE;
        @NotNull List<Project> projects = repository.findAll("user1", comparator);
        @NotNull List<Project> expected = Arrays.asList(gammaProject, alphaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void set() {
        @NotNull final List<Project> projects = Arrays.asList(new Project(), new Project());
        Assert.assertNotNull(repository.set(projects));
        Assert.assertEquals(projects, repository.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsById(alphaProject.getId()));
        Assert.assertFalse(repository.existsById("not-existing-project-id"));
    }

    @Test
    public void existsUserProjectById() {
        Assert.assertTrue(repository.existsById("user1", alphaProject.getId()));
        Assert.assertFalse(repository.existsById("user2", alphaProject.getId()));

        Assert.assertFalse(repository.existsById(null, betaProject.getId()));
        Assert.assertFalse(repository.existsById("user1", null));
    }

    @Test
    public void findOneById() {
        Assert.assertSame(alphaProject, repository.findOneById(alphaProject.getId()));
        Assert.assertNull(repository.findOneById("not-existing-project-id"));
    }

    @Test
    public void findOneUserProjectById() {
        Assert.assertSame(alphaProject, repository.findOneById("user1", alphaProject.getId()));
        Assert.assertNull(repository.findOneById("user2", alphaProject.getId()));

        Assert.assertNull(repository.findOneById(null, alphaProject.getId()));
        Assert.assertNull(repository.findOneById("user1", null));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertSame(betaProject, repository.findOneByIndex(1));
        Assert.assertNull(repository.findOneByIndex(999));
    }

    @Test
    public void findOneUserProjectByIndex() {
        Assert.assertSame(betaProject, repository.findOneByIndex("user2", 0));
        Assert.assertNull(repository.findOneByIndex("user2", 1));

        Assert.assertNull(repository.findOneByIndex(null, 0));
        Assert.assertNull(repository.findOneByIndex("user1", null));
    }

    @Test
    public void getSize() {
        Assert.assertEquals(3, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void getSizeOfUserProjects() {
        Assert.assertEquals(2, repository.getSize("user1"));
        Assert.assertEquals(0, repository.getSize(null));
    }

    @Test
    public void remove() {
        @Nullable final Project removedProject = repository.remove(alphaProject);
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(repository.existsById(alphaProject.getId()));

        Assert.assertNull(repository.remove(null));
    }

    @Test
    public void removeUserProject() {
        @Nullable Project removedProject = repository.remove("user1", alphaProject);
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(repository.existsById(alphaProject.getId()));

        removedProject = repository.remove("user1", betaProject);
        Assert.assertNull(removedProject);
        Assert.assertTrue(repository.existsById(betaProject.getId()));

        Assert.assertNull(repository.remove(null, gammaProject));
        Assert.assertNull(repository.remove("user1", null));
    }

    @Test
    public void removeById() {
        @Nullable final Project removedProject = repository.removeById(alphaProject.getId());
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(repository.existsById(alphaProject.getId()));

        Assert.assertNull(repository.removeById("not-existing-project-id"));
    }

    @Test
    public void removeUserProjectById() {
        @Nullable Project removedProject = repository.removeById("user1", alphaProject.getId());
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(repository.existsById(alphaProject.getId()));

        removedProject = repository.removeById("user1", betaProject.getId());
        Assert.assertNull(removedProject);
        Assert.assertTrue(repository.existsById(betaProject.getId()));

        Assert.assertNull(repository.removeById(null, gammaProject.getId()));
        Assert.assertNull(repository.removeById("user2", null));
    }

    @Test
    public void removeByIndex() {
        @Nullable final Project removedProject = repository.removeByIndex(0);
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(repository.existsById(alphaProject.getId()));

        Assert.assertNull(repository.removeByIndex(999));
    }

    @Test
    public void removeUserProjectByIndex() {
        @Nullable Project removedProject = repository.removeByIndex("user1", 1);
        Assert.assertSame(gammaProject, removedProject);
        Assert.assertFalse(repository.existsById(gammaProject.getId()));

        removedProject = repository.removeByIndex("user2", 1);
        Assert.assertNull(removedProject);

        Assert.assertNull(repository.removeByIndex(null, 0));
        Assert.assertNull(repository.removeByIndex("user2", null));
    }

    @Test
    public void removeByName() {
        @NotNull List<Project> removedProjects = repository.removeByName("user1", alphaProject.getName());
        Assert.assertEquals(1, removedProjects.size());
        Assert.assertFalse(repository.existsById(alphaProject.getId()));

        removedProjects = repository.removeByName("user1", "not-existing-project");
        Assert.assertTrue(removedProjects.isEmpty());
    }

}
