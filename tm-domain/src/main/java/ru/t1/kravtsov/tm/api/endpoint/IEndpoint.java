package ru.t1.kravtsov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IEndpoint {

    @NotNull
    String REQUEST = "request";

}
