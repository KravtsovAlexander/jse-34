package ru.t1.kravtsov.tm.dto.response;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public abstract class AbstractResultResponse extends AbstractResponse {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    public AbstractResultResponse(@NotNull final Throwable throwable) {
        setSuccess(false);
        setMessage(throwable.getMessage());
    }

}
