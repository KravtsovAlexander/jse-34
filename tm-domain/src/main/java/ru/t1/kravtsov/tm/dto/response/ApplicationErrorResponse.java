package ru.t1.kravtsov.tm.dto.response;

import io.swagger.annotations.ApiModel;
import org.jetbrains.annotations.NotNull;

@ApiModel
public class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
