package ru.t1.kravtsov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

}
