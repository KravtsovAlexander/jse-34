package ru.t1.kravtsov.tm.dto.response;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Task;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private Task task;

    public AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

}
