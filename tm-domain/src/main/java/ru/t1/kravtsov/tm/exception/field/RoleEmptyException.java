package ru.t1.kravtsov.tm.exception.field;

public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error. Role is empty.");
    }

}
